# Exercice
Nous disposons d'un dossier avec, pour chaque étudiant présent dans la salle, un fichier à son nom avec son nom et prénom écrit dedans.

À l'aide de File System (fs) (normal ou celui avec les Promises) :
* Lire tous les fichiers du dossier et stocker chacun des noms+prenom dans un tableau.
* Écrire ensuite, à partir du tableau, chacun des noms (à la ligne) dans un seul fichier listeEtudiant.txt
* On peut alors supprimer tous les fichiers des étudiants.

