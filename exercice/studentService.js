const fsPromise = require('fs/promises');
const path = require('path');

let students = ["Héloïse Andelhofs",
"Timothy Beek",
"Mathilde Bouckaert",
"Romain Bourgeois",
"Jean-Christophe Cartiaux",
"Tom Colin",
"Nicolas Gemine",
"Tony Jadoulle",
"Vanessa Lutgen",
"Alain Roos",
"Kevser SARUHAN",
"Maxime Turla"] //© Vanessa

const studentService = {
    settup : async() => {
        // ©Vanessa
        for(let i = 0;i<students.length;i++)
        {
            let firstName = students[i];
            const fileName = "./students/"+ firstName.replace(" ","_") + '.txt';
            try{
                const data = await fsPromise.writeFile(fileName,firstName);
                console.log("[FSPromise - Write] : SUCCESS - " + data);
            }
            catch(err){
                console.log("Erreur dans l'écriture de la promesse des données" + err)
            }

        }
    },

    createListFromFiles : async() => {
       
        //? -1 Effacer listeEtudiants.txt s'il existe
        try {
            //Vérifier si listeEtudiants.txt existe
            //Si oui, on le supprime
            const listeEtudiantPath = path.resolve(process.cwd(), 'students', 'listeEtudiants.txt');
            const stat = await fsPromise.stat(listeEtudiantPath);
            if(stat) {
                console.log('Le fichier existe, suppression de listeEtudiant');
                await fsPromise.unlink(listeEtudiantPath);
            }
        }
        catch(err) {
            console.log('Le fichier listeEtudiant n\'existe pas encore');
        }

        //? 0) Récupérer tous les fichiers du dossier
        const dirPath = path.resolve(process.cwd(), 'students');
        try {
            //Récupère tous les noms de fichiers présents dans le dossier passé en param
            const files = await fsPromise.readdir(dirPath);
            console.log(files);

            //? 1) Lire tous les fichiers du dossier et stocker chacun des noms+prenom dans un tableau.
            const tabStudent = [];

            //#region V1
            // for(const file of files) {
            //     //Lire le contenu du fichier
            //     const filePath = path.resolve(process.cwd(), 'students', file);
            //     const data = await fsPromise.readFile(filePath);
            //     //Le stocker dans le tableau
            //     tabStudent.push(data.toString());
            // } //Le soucis ici, est que toutes ces lectures se font de façon asynchrones et qu'à la sortie de la boucle, elles sont pas forcément encore finies
            // //afficher le tableau
            // console.log('Les students après boucle : ', tabStudent);
            //#endregion V1

            //#region V2
            const readPromises = [];
            for(const file of files) {
                const filePath = path.resolve(process.cwd(), 'students', file);
                //On stocke dans notre tableau de promesses, la promesse de lire chaque fichier
                readPromises.push(fsPromise.readFile(filePath));
            }
            //readPromises à la fin de la boucle est alors rempli de toutes les promesses de lecture
            // Je peux alors faire une Giga Promesse de toutes ces promesses
            // Promise.all(readPromises) -> Créer une grosse promesse avec toutes celles fournies en param
            const datas = await Promise.all(readPromises);
            //datas -> Tableau qui contient le résultat de chacune des promesses
            console.log(datas);
            // for(const data of datas){
            //     tabStudent.push(data.toString());
            // }
            // ... -> spread operator, permet de destructurer un tableau
            // En gros, c'est comme si j'enlevais les []
            tabStudent.push(...datas.map(data => data.toString()));
            console.log(tabStudent);
            //#endregion V2

            //? 2) Écrire ensuite, à partir du tableau, chacun des noms (à la ligne) dans un seul fichier listeEtudiant.txt
            const listStudentPath = path.resolve(process.cwd(), 'students', 'listeEtudiants.txt')
            const writePromises = [];
            for(const student of tabStudent){
                //Attention, on va utiliser le flag a pour ajouter le texte dans le fichier à chaque fois, sinon, ça va réécrire par dessus les noms chaque fois
                writePromises.push(fsPromise.writeFile(listStudentPath, student+'\n', { flag : 'a' }));
            }
            //On a rempli les promesses d'écriture avec la boucle, on va pouvoir toutes les lancer
            await Promise.all(writePromises);
            //Si on arrive ici, toutes nos promesses d'écritures ont réussi
            console.log("Ecriture dans le fichier listeEtudiants finie !");

            //? 3) On peut alors supprimer tous les fichiers des étudiants.
            for(const file of files){
                const filePath = path.resolve(process.cwd(), 'students', file);
                await fsPromise.unlink(filePath);
            }
            
        }
        catch(err){
            console.log("Erreur lors de l'exercice : ", err);
        }

    }
}

module.exports = studentService;