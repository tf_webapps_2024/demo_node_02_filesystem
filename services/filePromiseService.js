
const path = require('path');

const fsPromise = require('fs/promises');

const filePromiseService = {    
    read : async(fileName) => {
        //? 1 -> Faire le chemin jusqu'au fichier :
        const filePath = path.resolve(process.cwd(), 'files' , fileName);

        //? 2 -> Appeler read du fs
        try {
            const data = await fsPromise.readFile(filePath);
            console.log('[FSPromise - READ] : SUCCESS - ' + data );
        }
        catch(err){
            console.log('[FSPromise - READ] : ERROR');
        }
    },

    delete : async(fileName) => {
        const filePath = path.resolve(process.cwd(), 'files' , fileName);

        try {
            await fsPromise.unlink(filePath);
            console.log("[FS PROMISE - DELETE SUCCESS]");
        }
        catch(err){
            console.log("[FS PROMISE - DELETE ERROR]");
        }
    }

}

module.exports = filePromiseService;