
// Module qui aide à la gestion des chemins
const path = require('path');
// Module pour gérer la lecture de fichiers
const fs = require('fs');

const fileService = {
    read : (fileName) => {
        //? 1) On aura besoin du chemin vers le fichier, on peut s'aider du module path, pour ça
        // console.log(fileName);
        //process.cwd() -> current working directory : Chemin jusqu'au dossier de travail actuel (demo_02)
        // console.log(process.cwd()); 
        // path.resolve() -> créé un chemin absolue en joignant toutes les infos passées en paramètre
        // console.log( path.resolve(process.cwd(), 'files', fileName) );
        const filePath = path.resolve(process.cwd(), 'files', fileName);

        //? 2) On lance la lecture du fichier :
        //fs.readFile(cheminVersFichier, fonctionAExecuterQuandIlALu)
        console.log("[FS : READ] : Lancement")
        fs.readFile( filePath , (err, data) => {
            if(err) {
                console.log('[FS : READ] : Erreur à la lecture du fichier');
                return;
            }
            // data est un Buffer -> Mon texte sous forme de bytes
            console.log(data);
            // je dois utiliser toString() sur mon Buffer pour récupérer un texte
            console.log(data.toString());
            //Comme je concatène, le toString est déjà appliqué 
            console.log(`[FS : READ ] : ${data}`); 
        } );

    },

    write : (fileName, textToWrite) => {
        //? 1) Faire le chemin jusqu'au fichier
        const filePath = path.resolve(process.cwd(), 'files', fileName);

        //? 2) Appeler la fonction d'écriture du module fs
        // Par défaut, efface tout le fichier et réécrit par dessus
        console.log("[FS : WRITE] : Lancement ");
        const text = `${new Date().toLocaleTimeString()} : ${textToWrite}`;
        fs.writeFile(filePath,  text,  (err, data) => {
            if(err) {
                console.log('[FS : WRITE] : Erreur fichier ');
                return;
            }
            console.log('[FS : WRITE] : Ecriture terminée');
        })
    },

    open : (fileName, flags, textToWrite) => {
        //? 1 -> Créer chemin
        const filePath = path.resolve(process.cwd(), 'files', fileName);

        //? 2 -> Ouverture du fichier
        // fd -> file descriptor -> fichier ouvert
        console.log('[FS - OPEN] : Lancement')
        fs.open(filePath, flags, (err, fd) => {
            if(err) {
                console.log('[FS - Open] : Erreur');
                return;
            }
            console.log('[FS - Open] : Success');

            // fd -> le fichier
            // buffer -> le buffer dans lequel sera stocké les données
            // offset -> A partir de quel byte on commence dans le buffer
            // length -> Nombre de bytes qu'on va lire
            // position -> A partir de quel byte on commence dans le fichier
            // fs.read(fd, buffer, offset, length, position, ( ) => {})

            const buffer = Buffer.alloc(200);
            fs.read(fd, buffer, 0, buffer.length, 0, (err, nbBytesLus) => {
                if(err) {
                    console.log('[FS - OPEN - READ] : Error');
                    return;
                }
                console.log('[FS - OPEN - READ] : Success - Nb bytes lus ' + nbBytesLus);
                console.log(`[FS - OPEN - READ] : Success - ${buffer.toString()}`);
            });

            //On vérifie si un texte a été fourni pour savoir si on doit écrire ou pas
            if(textToWrite) {
                fs.write(fd, textToWrite, (err) => {
                    if(err) {
                        console.log('[FS - OPEN - WRITE] : Error');
                        return;
                    }
                    console.log('[FS - OPEN - WRITE] : Success');
                });
            }
        })
    }
}


module.exports = fileService;